console.log("hello")

let trainer ={
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pichachu","Charizard","Squirtle","Bulbasaur"],
	friends:{
		hoenn: ['May','Max'],
		kanto: ['Brock','Misty']
	},
	talk: function(){
		console.log('Picachu I choose you!');
		
	}

}
console.log(trainer);
console.log("Result of dot notation :");
console.log(trainer.name)
console.log("Result of bracket notation :");
console.log(trainer['pokemon'])
console.log("Result of talk method :");
trainer.talk();



function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attact = level;

	// methods 
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health = target.health - this.attack;
		console.log(target.name +'s health is now reduce to' + target.health);
	};	
	// if (target.health <= 0) {
	// 		console.log(target.name + " fainted");
	// 	}
	// 	console.log(target);

	// this.faint = function(){
	// 	console.log(this.name + 'fainted');
	// }
}

let pikachu = new Pokemon("Pikachu",12);
console.log(pikachu);

let geoudue = new Pokemon("Geodude",8);
console.log(geoudue);

let mewtwo = new Pokemon("Mewtwo",100);
console.log(mewtwo);

geoudue.tackle(pikachu);
mewtwo.tackle(geoudue);

console.log(geoudue);